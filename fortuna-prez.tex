\documentclass[9pt,notes]{beamer}
\usepackage[T1]{fontenc}
% \usepackage[utf8]{inputenc} % alleged compat. issues with fontenc
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{xmpincl}
\usepackage{hyperxmp}

\usepackage{fontspec}
\setmonofont{Fira Code Retina}[
  Contextuals=Alternate  % Activate the calt feature
]
\usepackage{listings}
\usepackage{lstfiracode} % https://ctan.org/pkg/lstfiracode
\lstset{
  language=C++,
  style=FiraCodeStyle,   % Use predefined FiraCodeStyle
  basicstyle=\ttfamily   % Use \ttfamily for source code listings
}
\usefonttheme[onlymath]{serif} % proper math font

\makeatletter
\def\input@path{{./dracula-beamer/}}
\makeatother

\mode<beamer> {
\usetheme[width=0pt]{Hannover}
\usecolortheme{dracula}
\usepackage{graphicx}
}

\setbeamertemplate{footline}[frame number]
\setbeamertemplate{caption}[numbered]
\stepcounter{subsection}
\useinnertheme{circles}
\useoutertheme[compress,subsection=false]{miniframes}


\definecolor{mysmootherblue}{HTML}{7590DB}

% these are kanged from dracula beamer theme
\definecolor{draculafg}      {RGB} {248,  248,  242}
\definecolor{draculacomment} {RGB} {98,   114,  164}
\definecolor{draculacyan}    {RGB} {139,  233,  253}
\definecolor{draculagreen}   {RGB} {80,   250,  123}
\definecolor{draculapink}    {RGB} {255,  121,  198}
\definecolor{draculapurple}  {RGB} {189,  147,  249}
\definecolor{draculayellow}  {RGB} {241,  250,  140}

\lstdefinestyle{customcppstyle}{
  language=c++,
  extendedchars=true,
  inputencoding=utf8,
  keepspaces=true,
  upquote=true,
  % frame=L,
  keywordstyle = {\color{draculapink}},
  keywordstyle = [2]{\color{draculapurple}},
  keywordstyle = [3]{\color{mysmootherblue}},
  stringstyle = {\color{draculayellow}},
  commentstyle = {\color{draculacomment}},
  directivestyle={\color{mysmootherblue}},
  identifierstyle=\color{draculafg},
  deletekeywords={const,mutable},
  morekeywords = [2]{std,fortuna,fmt,CryptoPP,load,wait,count_down,read,gcount,call_once},
  morekeywords = [3]{new,delete,this},
  emph = {int,char,double,float,unsigned,void,const,mutable,bool,int64_t,uint64_t,size_t,once_flag,latch,string,runtime_error,mutex,lock_guard,unique_lock,ifstream,ios},
  emphstyle={\ttm \color{draculagreen}},
  numbers=none,
  % stepnumber=5,
  % numbersep=3pt,
  % numberstyle=\tiny\color{draculapurple},
  tabsize=4,
  showspaces=false,
  showtabs=false,
  showstringspaces=false
}


\graphicspath{{./img/}}
\renewcommand{\figurename}{Obrázok}

\date{\today}

\title{Fortuna}
\author[AM]{Adam Mirre\inst{1}\\(\href{https://git.dotya.ml/wanderer}{@wanderer})}

\institute[FAI]
{
  \inst{1}
  Fakulta Aplikované Informatiky\\
  UTB ve Zlíně
}

% metadata
\hypersetup{
    pdftitle={Fortuna},
    pdfauthor={Adam Mirre (https://git.dotya.ml/wanderer)},
    pdfsubject={a take on the CSPRNG Fortuna devised by Niels Fergusson, Bruce Schneier \& Tadayoshi Kohno (https://git.dotya.ml/ak-fortuna/fortuna)},
    pdfkeywords={cryptography, Fortuna, CSPRNG, randomness, entropy, security, c++, multi-threading},
    pdflang=la,
    pdfapart=3,
    pdfaconformance=B,
    pdflang={en},
    % use breaklinks to make have long bibtex urls broken
    % otherwise they go over the margin (known issue)
    breaklinks=true
}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

\frame[plain]{\titlepage}

\begin{frame}
\frametitle{Obsah}
  \tableofcontents
\end{frame}

\begin{frame}
\frametitle{Úvod}
  \begin{block}{Témy prezentácie}
    \begin{itemize}
      \item kultúrny kontext
      \item konceptuálna skladba Fortuny
      \item konkrétne detaily implementácie
      \item detaily použitých validačných metód
    \end{itemize}
  \end{block}
\end{frame}

\section{Fortuna CSPRNG}
\subsection{Kultúrny kontext}
\begin{frame}{Figurálne zobrazenia Fortuny v dejinách umenia}
  \begin{columns}[b]
    \begin{column}{0.6\textwidth}
      \begin{figure}[b]
      \includegraphics[height=.79\textheight]{La_Fortune}
      \hfill
      \caption{Charles Samuel: La Fortune (1894) \cite{ChSamuel}}
      \label{lafortune}
      \end{figure}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{exampleblock}{Fortuna}
        \begin{itemize}
          \item rímska bohyňa náhody, osudu, hojnosti a personifikácia šťastia
            (alebo nešťastia), má v rukách moc ovplyvniť smerovanie ľudského
            života
          \item pôvod v gréckej Tyché, podobnosť s egyptskou Isis
        \end{itemize}
      \end{exampleblock}
      % \vspace{5mm}
      \medskip
    \end{column}
  \end{columns}
\end{frame}
\begin{frame}[plain]
      \begin{figure}[hbt]
      \centering
      \includegraphics[width=.49\textwidth]{Fortuna_Vienna}
      \caption{Fortuna auf der Neuen Burg, Wien \cite{NeuenBurg}}
      \label{fortunavienna}
      \end{figure}
\end{frame}


\subsection{Intro}
\begin{frame}
\frametitle{Fortuna}
\framesubtitle{Intro}
  \begin{columns}[t]
    \begin{column}{0.7\textwidth}
  \begin{block}{Fortuna \cite{fortuna}}
    \begin{itemize}
      \item kryptograficky bezpečný generátor pseudo-náhodných čísel (CSPRNG)
        definovaný v knihe \alert{\textit{Cryptography Engineering}}, resp.
        prvom vydaní pod názvom \textit{Practical Cryptography}
    \end{itemize}
  \end{block}
  \begin{block}{súčasti}
    \begin{itemize}
      \item generátor
      \item akumulátor
      \item manažér seed súborov
      \item zdroje entropie
      \item vonkajšie rozhranie
    \end{itemize}
  \end{block}
    \end{column}
    \begin{column}{0.3\textwidth}
      \begin{figure}[!hbt]
      \centering
      \includegraphics[width=.85\textwidth]{bookcover}
      \caption{Cryptography Engineering: \textit{Design Principles and
      Practical Applications}}
      \label{bookcover}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\section{Generátor}
\begin{frame}
\frametitle{Generátor}
\framesubtitle{štruktúra}
  \begin{block}{stav generátora}
    interný objekt.\\
    implementovaný ako štruktúra (POD\footnotemark) \alert{\texttt{G\_state}}, ktorá
    obsahuje:
    \begin{itemize}
      \item 256 bitový kľúč \alert{\texttt{k}}
      \item 128 bitové počítadlo \alert{\texttt{ctr}}, v praxi pole bytov
    \end{itemize}

    \begin{figure}[!hbt]
    \centering
    \includegraphics[width=.7\textwidth]{g_state}
    \caption{Štruktúra stavu generátora G\_state}
    \label{gstate}
    \end{figure}
  \end{block}

  \footnotetext[1]{\textit{angl.} Plain Old Data structure \rightarrow \texttt{struct}}
\end{frame}

\begin{frame}
\frametitle{Generátor}
  \begin{block}{úlohy}
    \begin{itemize}
      \item prijímať požiadavky na generovanie náhodných dát
      \item generovať náhodné dáta za pomoci šifrovacej funkcie
      \begin{itemize}
        \item na konci spracovania požiadavky vytvoriť nový kľúč, starý zničiť
          (najneskôr po $2^{20}$ bytoch náhodných dát)
      \end{itemize}
    \end{itemize}

    konkrétna implementácia v triede \texttt{Generator}
  \end{block}
\end{frame}

\begin{frame}[t]
\frametitle{Generátor}
\framesubtitle{štruktúra}
  \begin{block}{využitie kryptografických primitív}
    \vspace*{-1.5\baselineskip}
    \begin{columns}
      \begin{column}{0.8\textwidth}
        \begin{itemize}
          \item bloková "AES-like" \cite[143]{CryptEngi} šifra -
            \alert{Serpent} - použitá v~CTR\footnotemark móde
          \begin{itemize}
            \item G.ctr slúži ako IV (inicializačný vektor, \textit{angl.} \textbf{I}nitialization \textbf{V}ector)
            \item G.k slúži ako kľúč (256 bit)
          \end{itemize}
          \item SHA256 hashovacia funkcia
        \end{itemize}
      \end{column}
      \begin{column}{0.2\textwidth}
        \begin{figure}[hbt]
          \centering
          \includegraphics[width=1\textwidth]{serpent}
          \caption{maskot šifry Serpent \cite{Serpent}}
          \label{serpent}
        \end{figure}
      \end{column}
    \end{columns}
  \end{block}
  \begin{exampleblock}{CTR mód \cite[70]{CryptEngi}}
    \vspace*{-3\baselineskip}
    \[\flushleft
      K_i := E(K, Nonce \| i) \quad \textrm{for} \: i = 1,...,k\\
      C_i := P_i \oplus K_i
    \]
    \vfill
  \end{exampleblock}

  \footnotetext{Counter}
\end{frame}

\begin{frame}
\frametitle{Generátor}
\framesubtitle{}
  \begin{block}{požiadavky}
    \begin{itemize}
      \item prístup k stavu generátora nie je možný z vonkajších funkcií
        (\alert{zapúzdrenie})
      \item generovanie náhodných dát je možné až po prvom zaseedovaní
      \item reseed \cite[159]{CryptEngi} môže prebehnúť vždy keď:
      \begin{itemize}
        \item $P_{0}$ má dĺžku aspoň \texttt{MIN\_POOL\_SIZE}
          \cite[149]{CryptEngi} (\alert{64 bytov} \cite[154]{CryptEngi})
        \item od posledného reseedu ubeho aspoň \alert{100ms}
          \cite[150]{CryptEngi}
      \end{itemize}
      \item starý kľúč je pri obnove nenávratne zničený ("forward secrecy")
        \cite[147]{CryptEngi}
      \item rovnaký kľúč je použitý na vygenerovanie vždy maximálne $2^{20}$ bytov dát
    \end{itemize}
  \end{block}
\end{frame}

\section{Akumulátor}
\begin{frame}
\frametitle{Akumulátor}
\framesubtitle{štruktúra}
  \begin{block}{súčasti}
    \begin{itemize}
      \item pooly
      \begin{itemize}
        \item $Pool_{0-31}$
      \end{itemize}
      \item počítadlo \texttt{reseed\_ctr}
      \item zdroj(e) entropie
    \end{itemize}
  \end{block}

  \begin{block}{úlohy}
    \begin{itemize}
      \item \alert{zber} udalostí s entropiou
      \item \alert{distribúcia} udalostí s entropiou na pooly
      \item \alert{kontrola} validity udalostí (minimálna/maximálna povolená dĺžka)
      \item facilitácia \alert{prístupu} ku generátoru pre SeedFileManager
        (\texttt{reseed()}, \texttt{random\_data()})
    \end{itemize}

    konkrétna implementácia v triede \texttt{Accumulator}
  \end{block}
\end{frame}

\begin{frame}
\frametitle{Akumulátor}
\begin{exampleblock}{\texttt{reseed\_ctr}}
  \begin{itemize}
    \item vynulovaný pri \alert{inicializácii PRNG} (pri štarte programu)
    \item drží počet volaní \alert{\texttt{reseed()}} funkcie Generátora
    \item ovplyvňuje voľbu poolov použitých pri reseede
  \end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}[plain]
  \begin{figure}[!hbt]
  \centering
  \includegraphics[width=.70\textwidth]{reseed-ctr-incr}
  \caption{Inkrementácia reseed\_ctr}
  \label{reseedctr}
  \end{figure}
\end{frame}

\begin{frame}
\frametitle{Akumulátor}
% \framesubtitle{Pool}
\framesubtitle{}
  \begin{block}{Pool}
    \begin{itemize}
      \item obsahuje reťazec znakov potenciálne neobmedzenej dĺžky
        \cite[148]{CryptEngi}
      \item identifikovaný na základe priradenej hodnoty \alert{id}
      \item reťazec poolu - \alert{\texttt{s}} - sa používa na jedinú vec: ako
        vstup do hashovacej funkcie pred reseedom \cite[148]{CryptEngi}
      \item po použití reťazca je pool vždy "vyčistený" na prázdny reťazec
      \item logika výberu poolov na žatvu (harvest) pred reseedom \cite[154]{CryptEngi}:
      \begin{itemize}
      \item $P_{i}$ je použitý ak $2^i$ je deliteľom
        \texttt{\alert{reseed\_ctr}}, teda $P_{0}$ je použitý zakaždým, $P_{1}$
        každý druhý reseed ($2^1$), $P_{2}$ ak \texttt{resed\_ctr==}$2^2$, etc.
      \end{itemize}
    \end{itemize}

    konkrétna implementácia v triede \texttt{Pool}
  \end{block}
\end{frame}

\begin{frame}[plain]
  \begin{figure}[hbt]
  \centering
  \includegraphics[width=.9\textwidth]{pool-append-fun}
  \caption{Členská funkcia pre pridanie udalosti do reťazca poolu "s"}
  \label{appendpool}
  \end{figure}
\end{frame}

\begin{frame}
\frametitle{Akumulátor}
\framesubtitle{Generický zdroj entropie}
  \begin{block}{EntropySrc}
    \begin{itemize}
      \item má priradené \alert{id}
      \item generuje/z pohľadu Fortuny poskytuje \alert{udalosti} s entropiou
      \item identifikuje sa svojím id z intervalu $<0,255>$ pri pridávaní
        udalostí do Poolu
      \item udalosti distribuuje na pooly \alert{cyklickým} spôsobom
        \cite[148]{CryptEngi}
      \item dôležitá je \emph{nepredvídateľnosť} produkovaných udalostí
    \end{itemize}

    definované v abstraktnej triede \texttt{EntropySrc}

    \begin{itemize}
      \item príklady vhodných zdrojov entropie:
        \begin{itemize}
          \item držanie klávesov v µs/ns
          \item fluktuácie úrovní napätia na perifériách
          \item seek-time hláv magnetických diskov
        \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
\frametitle{Akumulátor}
\framesubtitle{Udalosti s entropiou (a ich distribúcia)}
  \begin{block}{udalosť}
    \begin{itemize}
      \item náhodná postupnosť znakov o veľkosti \alert{$1-32$ bytov}
        generovaná/poskytovaná \textit{zdrojom entropie}
      \item pred pridaním do Poolu je \alert{enkódovaná} ako "source\_id +
        event\_size + event"
      \item enkódovaná udalosť (metadáta + samotná udalosť s entropiou) je pred
        pridaním do Poolu \alert{zahashovaná} pomocou SHA256
    \end{itemize}
    práve spomínaný hash \textit{digest} je následne pridaný do reťazca konkrétneho Poolu
  \end{block}
\end{frame}

\begin{frame}
\frametitle{Akumulátor}
\framesubtitle{Udalosti s entropiou (a ich distribúcia) 2}
  \begin{block}{zdroj}
    \begin{itemize}
      \item určuje cieľový pool
      \item rotuje cieľové pooly po každej udalosti
      \item produkuje udalosť s entropiou o veľkosti $1-32$ bytov
    \end{itemize}
  \end{block}

  \begin{block}{podmienky}
    \begin{itemize}
      \item udalosť by mala mať čo najväčšiu entropiu, preto nemá význam
        spracovávať napr. temporálne hodnoty ako aktulny deň, mesiac, rok;
        tieto vedomosti bude mať jednoducho aj útočník
      \item použitie udalostí s dĺžkou $>$32bytov zvyšuje štatistickú deviáciu
        od čistej náhodnosti \cite[143]{CryptEngi}, preto je \textbf{max. limit
        dĺžky udalosti} \alert{32 bytov}
    \end{itemize}
    Fortuna by mala mať dostupných, pokiaľ možno, viacero zdrojov entropie
  \end{block}
\end{frame}


\section{Správa seed súborov}
\begin{frame}
\frametitle{Správa seed súborov}
  \begin{block}{úlohy}
    \begin{itemize}
      \item periodický \alert{zápis} nazbieranej entropie na disk (pre neskoršie
        použitie)
      \item \alert{obnovenie} entropie zo súboru na disku po opätovnom spustení
      \item \alert{zaseedovanie} generátora (skrze akumulátor) po obnovení
        prevádzky programu
    \end{itemize}
    implementované v triede \texttt{SeedFileManager}
  \end{block}

  \begin{block}{podmienky práce}
    \begin{itemize}
      \item veľkosť seed súboru je \alert{64 bytov} (konfigurovateľné)
      \item používateľ má právo zapisovať do priečinka, kde má byť vytvorený
        seed súbor
    \end{itemize}
  \end{block}
\end{frame}

\section{Fortuna}
\begin{frame}
\frametitle{Vonkajšie rozhranie Fortuny}
  \begin{block}{účel}
    \begin{itemize}
      \item \alert{enkapsulovať} interné súčasti (napr. Generátor a jeho stav)
        a členské funkcie
      \item \alert{spravovať} vlákna služieb (napr. zdrojov entropie, SeedFileManager)
    \end{itemize}
    implementované v triede \texttt{Fortuna}
  \end{block}
\end{frame}


\section{Implementácia}
\begin{frame}
\frametitle{Implementácia}
  \framesubtitle{}
  \begin{block}{verziovaný kód + continuous builds}
    \begin{itemize}
      \item \alert{\href{https://gitea.io/}{Gitea}}
        \url{https://git.dotya.ml/ak-fortuna/fortuna}
      \item \alert{\href{https://www.drone.io/}{Drone CI}}
        \url{https://drone.dotya.ml/ak-fortuna/fortuna}
    \end{itemize}
  \end{block}
  \begin{block}{použité technológie}
    \begin{itemize}
      \item C++ 20
      \item \alert{\href{https://cryptopp.com}{Cryptopp}} (dep)
      \item \alert{\href{https://fmt.dev}{\{fmt\}}} (dep)
    \end{itemize}
    tooling:
    \begin{itemize}
      \item \texttt{\alert{\href{https://cmake.org/}{CMake}} cmake version
        3.22.2} +
        \texttt{\alert{\href{https://ninja-build.org/}{Ninja}} 1.10.2}
      \item \texttt{\alert{\href{https://gcc.gnu.org/}{g++}} (GCC) 11.2.1
        20212701} +
        \texttt{\alert{\href{https://lld.llvm.org/}{lld}} LLD 13.0.0}

      \item \texttt{\alert{\href{https://www.vim.org/}{Vim}} Vi IMproved 8.2,
        Included patches: 1-4232}
      \item \texttt{\alert{\href{https://getfedora.org/}{Fedora}} Linux 35}
      \item \texttt{The Linux Kernel with
        \alert{\href{https://copr.fedorainfracloud.org/coprs/rmnscnce/kernel-xanmod/}{XanMod
      Patches}} and experimental options version 5.16.5}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[plain]
  \framesubtitle{}
  \begin{figure}[hbt]
  \centering
  \includegraphics[width=.9\textwidth]{gitea}
  \caption{Zdrojový kód v repozitári}
  \label{gitea}
  \end{figure}
\end{frame}

\begin{frame}
\frametitle{Implementácia - výzvy}
\framesubtitle{}
  \begin{block}{Výzvy implementácie}
    \begin{itemize}
      \item paralelný beh viacerých častí programu
      \item konkurentný (súbežný) prístup
      \item spracovávanie signálov (\texttt{man 7 signal})
      \item kompilačné časy (C++)
      \item čistý kód bez varovaní aj so zapnutím vysokých úrovní
        diagnostiky:\\ \alert{\texttt{-Wall -Wextra -Wpedantic}}
      \item kompilácia s \alert{\texttt{-pedantic}} pre kód vyhovujúci ISO C++
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
\frametitle{Implementácia - riešenia}
\framesubtitle{}
  \begin{block}{Riešenia}
    \begin{itemize}
      \item synchronizačné mechanizmy
      \begin{itemize}
        \item \texttt{std::mutex}
        \item \texttt{std::lock\_guard} a \texttt{std::unique\_lock}
        \item \texttt{std::latch}
        \item \texttt{std::once\_flag} a \texttt{std::call\_once}
      \end{itemize}
      \item signal handler
        \begin{itemize}
          \item maskovanie signálov (\texttt{SIGTERM, SIGINT})
        \end{itemize}
      \item kompilácia v \texttt{/tmp} (ramdisk)
      \item využívanie korektného C++
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[t,fragile]
\frametitle{Ochrana bloku kódu pred konkurentným prístupom}
\framesubtitle{\texttt{std::mutex + std::lock\_guard}}
\lstset{basicstyle=\scriptsize\selectfont\ttfamily,style=customcppstyle}
\begin{lstlisting}
// fortuna.cpp
...
auto Fortuna::random_data(unsigned int n_bytes) -> void {
	if (!this->continue_running.load()) {
		return;
	}

	std::lock_guard<std::mutex> lg(mtx_random_data);
...
\end{lstlisting}
\end{frame}

\begin{frame}[t,fragile]
\frametitle{Čakanie worker vlákien na úvodnú inicializáciu CSPRNG}
\framesubtitle{\texttt{std::latch}}
\lstset{basicstyle=\scriptsize\selectfont\ttfamily,style=customcppstyle}
\begin{lstlisting}
// fortuna.h
...
private:
	std::latch sync_point{1};
...
	auto initialize_prng() -> void {
		try {
...
			this->sync_point.count_down(); // prove we've been here
		}
...
\end{lstlisting}

\begin{lstlisting}
// fortuna.cpp
...
Fortuna::Fortuna() {
	try {
...
		this->sync_point.wait(); // wait for init
		// run threads now
...
	}
\end{lstlisting}
\end{frame}

\begin{frame}[t,fragile]
\frametitle{Zaistenie behu inicializačnej funkcie len jeden krát}
\framesubtitle{\texttt{std::once\_flag + std::call\_once}}
\lstset{basicstyle=\scriptsize\selectfont\ttfamily,style=customcppstyle}
\begin{lstlisting}
// fortuna.h
...
private:
	std::once_flag PRNG_init;
...
	auto initialize_prng() -> void {
		try {
			std::call_once(PRNG_init, [] { ; });
...
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Zdroj entropie: UrandomEntropySrc}
  \begin{block}{}
    \begin{enumerate}
      \item vytvorený ako preddefinovaný zdroj
      \item spustený vo vlastnom vlákne v konštruktore \texttt{Fortuna} triedy
      \item beží ako služba na pozadí
      \item pridáva udalosti každých {\raise.17ex\hbox{$\scriptstyle\sim$}}50 ms
      \item de facto sekundárny zdroj: entropiu získava priamo z
        \texttt{/dev/urandom}
    \end{enumerate}
  \end{block}
  \begin{figure}[!hbt]
  \centering
  \includegraphics[width=1.00\textwidth]{ues}
  \caption{Náhodná udalosť od UrandomEntropySrc}
  \label{uesevent}
  \end{figure}
\end{frame}

\begin{frame}[t,fragile]
\frametitle{Aktualizovanie seed súborov po reštarte}
\framesubtitle{\texttt{čítanie súboru + reseed}}
\lstset{basicstyle=\scriptsize\selectfont\ttfamily,style=customcppstyle}
\begin{lstlisting}
// seed_file_management.cpp
...
	std::ifstream f_stream{config.seed_f_path, std::ios::binary};
	if (!f_stream.is_open()) {
		const std::string msg{"\t[!] sfm: error opening seed file!\n"};
		fortuna::SeedFileManager::IS_RUNNING = false;
		throw std::runtime_error(msg);
	}
	f_stream.read(reinterpret_cast<char*>(buff.BytePtr()),
				  static_cast<int64_t>(config.seed_f_length));
	if (static_cast<std::size_t>(f_stream.gcount()) != config.seed_f_length) {
		const std::string msg{
			"\t[!] sfm: error reading seed from file, length mismatch"};
		fortuna::SeedFileManager::IS_RUNNING = false;
		throw std::runtime_error(msg);
	}
}

std::string str_buff(reinterpret_cast<const char*>(&buff[0]),
					 buff.SizeInBytes());

this->_p_accumulator->call_reseed(str_buff);
write_seed_file();
...
\end{lstlisting}
\end{frame}

\section{Validácia}
\subsection{ENT}
\begin{frame}
\frametitle{Analýza výstupu Fortuny - štatistické testy ent}
\framesubtitle{Generovanie dát}
  \begin{block}{fortuna | dd}
    \vspace*{-.5\baselineskip}
    \begin{figure}[!hbt]
    \centering
    \includegraphics[width=1.0\textwidth]{fortuna2dd}
    \label{fortuna2dd}
    \end{figure}
    \vspace*{-.5\baselineskip}
    \begin{itemize}
      \item získame \alert{1000MB} pseudo-náhodných dát pomocou príkazu
        \texttt{./cmake-build-debug/fortuna | dd iflag=fullblock
        of=/tmp/rand1000M bs=1M count=1000 status=progress}, (obr.
        \ref{ddprepare}), kde výstup Fortuny vstupuje cez \alert{\texttt{|}} do
        programu \alert{\texttt{dd}}
      \item parameter \alert{\texttt{iflag=fullblock}} zaručuje, že
        \alert{\texttt{dd}}
        zapíše do súboru len plné bloky, čo znamená, že ak Fortuna nedodá naraz
        plný blok o veľkosti \alert{\textit{bs}} (block size), \texttt{dd} bude
        bufferovať a blok zapíše až vo chvíli, keď sa buffer naplní
    \end{itemize}
  \end{block}
  \begin{figure}[!hbt]
  \centering
  \includegraphics[width=.80\textwidth]{ddprepare100M}
  \caption{Príprava materiálu na testovanie - 1000MiB}
  \label{ddprepare}
  \end{figure}
\end{frame}

\begin{frame}
\frametitle{Analýza výstupu Fortuny - štatistické testy ent}
\framesubtitle{Testovanie - ENT}
  \begin{block}{ENT - A Pseudorandom Number Sequence Test Program \cite{ent}}
    \begin{itemize}
      \item spustíme testovanie príkazom \texttt{./ent /tmp/rand1000M}
        (obr.~\ref{enttest1000M}), kde:
      \begin{itemize}
        \item spustiteľný súbor \alert{\texttt{ent}} sa nachádza v aktuálnom
          adresári
        \item \alert{\texttt{/tmp/rand1000M}} je cesta k súboru s výstupom
          Fortuny, ktorý bol vytvorený v predchádzajúcom kroku (obr.
          \ref{ddprepare})
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{figure}[!hbt]
  \centering
  \includegraphics[width=.70\textwidth]{ent1000M}
  \caption{ent test vzorky 1000MiB dát Fortuny}
  \label{enttest1000M}
  \end{figure}
\end{frame}

\subsection{Dieharder}
\begin{frame}[t]
\frametitle{Analýza výstupu Fortuny - testovacia sada Dieharder}
  \begin{block}{Dieharder - A Random Number Test Suite \cite{dieharder}}
    sada testov \emph{generátorov náhodných čísel}.
    \medskip
    \\nástroj:
    \begin{itemize}
      \item určený pre generátory, nie \emph{"súbory možno-náhodných čísel"}
        \cite{dieharder}
      \item navrhnutý, aby pomohol \emph{slabý} generátor postrčiť k
        jednoznačnému zlyhaniu (na napr. hladine 0.0001\%), mimo limbo 1\%
        alebo 5\%-ného \emph{možno-zlyhania}
      \item okrem originálnych testov obsahuje množstvo testov z STS\footnotemark
  \footnotetext{Statistical Test Suite amerického National Institute for
  Standards and Technology (NIST)}
      \item napísaný v jazyku C
      \item licencované s \alert{GPL}\footnotemark
    \end{itemize}
    \medskip
    použitá verzia: \texttt{dieharder version 3.31.1}
  \end{block}

  \footnotetext{GNU General Public License}
\end{frame}

\begin{frame}[t,allowframebreaks]
\frametitle{Analýza výstupu Fortuny - testovacia sada Dieharder}
\framesubtitle{Testovanie - nastavenia, parametre}
  \begin{block}{}
    plný príkaz: \alert{\texttt{./cmake-build-release/fortuna | pv - | dieharder -a -g 200 -k 2 -Y 1}}
    \medskip
    \\použité \texttt{dieharder} parametre a ich význam:
    \begin{description}
      \item \alert{\texttt{-a}} spustí všetky testy
      \item \alert{\texttt{-g 200}} špeciálny mód pre čítanie zo štandardného
        vstupu (\texttt{-})
      \item \alert{\texttt{-k 2}} \textit{"ks\_flag"} - indikuje rýchlosť a
        presnosť
        čítania vzoriek
      \item \alert{\texttt{-Y 1}} \textit{"Xtrategy"} v móde \alert{"resolve
        ambiguity"}
        (RA) (viď obr. \ref{dieharderxtrategy})
        \begin{itemize}
          \label{dieharderRA}
          \item úlohou je \emph{jednoznačne} rozhodnúť o výsledku ak test
            skončí ako "WEAK" (slabý); RA mód opakovane pridáva \emph{psamples}
            (väčšinou v blokoch po 100) dokiaľ nie je výsledok testu stabilne
            silný alebo postupuje k jednoznačnému zlyhaniu
        \end{itemize}
    \end{description}
  \end{block}
  \begin{figure}[!hbt]
  \centering
  \includegraphics[width=.90\textwidth]{dieharder-xtrategy}
  \caption{man dieharder, sekcia \texttt{-Y}}
  \label{dieharderxtrategy}
  \end{figure}
\end{frame}

\begin{frame}[t]
\frametitle{Analýza výstupu Fortuny - testovacia sada Dieharder}
\framesubtitle{Testovanie - zapojenie komponent}
  \begin{block}{}
    \begin{enumerate}
      \item \alert{\texttt{fortuna}} (Release verzia) v móde
        \emph{kontinuálneho výstupu} na \texttt{stdout}, poskytuje prúd
        náhodných čísel
      \item \alert{\texttt{pv -}} metrika rýchlosti výstupu
      \item \alert{\texttt{dieharder}} analýza výstupu Fortuny
    \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}[t,allowframebreaks]
\frametitle{Analýza výstupu Fortuny - testovacia sada Dieharder}
\framesubtitle{Testovanie - výsledok}
  \begin{block}{výsledok (obr.~\ref{dieharderfulltestsuite})}
    \begin{itemize}
      \item \alert{234GiB} pseudonáhodných dát vygenerovaných Fortunou
      \item \alert{17h 36m 3s} testovania testovacou sadou Dieharder
      \item \alert{PASSED} (úspešne prejdený) pre všetky testy v móde
        \emph{resolve ambiguity} (viď s.~\ref{dieharderRA})
    \end{itemize}
  \end{block}

  \begin{figure}[!hbt]
  \centering
  \includegraphics[width=.57\textwidth]{dieharder-full-testsuite-run-proof}
  \caption{Dieharder - beh plnej testovacej sady; zobrazená len časť výsledkov}
  \label{dieharderfulltestsuite}
  \end{figure}
\end{frame}

\begin{frame}
\frametitle{Analýza výstupu Fortuny - testovacia sada Dieharder}
\framesubtitle{Testovanie - výsledok - asciinema}
  \begin{exampleblock}{asciicast\footnotemark}
    \texttt{asciinema play}a
    \alert{\url{https://dotya.ml/files/e/7wU3raPkP4DkzUeM-fortuna_dieharder_proof.cast}}\footnotemark
    \bigskip
    \\\alert{\url{https://asciinema.org/a/y5f7lLFWsDdztArXszCcseRIS}}\footnotemark^{\footnotemark\footnotemark}
  \end{exampleblock}

  \footnotetext[5]{asciinema \emph{[as-kee-nuh-muh]} - "a free and open source
  solution for recording terminal sessions...": \url{https://asciinema.org/}}
  \footnotetext[6]{príkazom sa nahrávka prehrá lokálne (je potrebné mať
  nainštalovaný program \texttt{asciinema})}
  \footnotetext[7]{nahrávka sa prehrá vo webovom prehrávači}
  \footnotetext[8]{pre neskreslené zhliadanie odporúčam režim FULLSCREEN}
  \footnotetext[9]{nič\footnote{\protect\footnotemark}}
  \footnotetext[10]{\url{https://xkcd.com/1208}}
\end{frame}


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Záver}

\begin{frame}
\frametitle{Budúce výzvy projektu}
\begin{block}{}
  \begin{itemize}
    \item spracovávanie vstupných parametrov
    \item "knižnicová" funkcionalita
    \item logovanie do súboru
  \end{itemize}
\end{block}
\end{frame}

\begin{frame}[t,allowframebreaks]
  \frametitle{Referencie}
  \fontsize{7pt}{8pt}\selectfont
  \bibliographystyle{plain}
  \bibliography{refs}
\end{frame}


\begin{frame}[plain]
\centering
Vďaka za pozornosť!
\end{frame}

\end{document}

% vim: ff=unix tw=0 wrap
